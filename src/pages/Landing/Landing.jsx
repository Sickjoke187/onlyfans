import React from 'react';
import useViewport from '../../hooks/Viewport';
import SignIn from '../../components/SignIn';
import '../../assets/styles/main.scss'
import Footer from '../../components/Footer';
import phones from '../../assets/img/phones.png';
import fan3 from '../../assets/img/fan3.png';

function SwiperMcSwipeWrapper() {
  return (
    <div id="phonesWrapper">
      <div className="untitledClassName">
        <img src={phones} id="phonesImg"/>
        <div id="moose">
          <img src={fan3} id="phoneScreen" />
        </div>
      </div>
    </div>
  )
}

export default function Landing() {

  const { width, height } = useViewport();

  return (
    <div className="Container">
      <div className="LandingContainer">
        {
          width >= 902 && (
            <div className="phone_col">
              <SwiperMcSwipeWrapper />
            </div>
          )
        }
        <SignIn />
      </div>
      <Footer />
    </div>
  )
  
}